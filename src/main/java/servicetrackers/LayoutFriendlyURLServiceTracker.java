package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import com.liferay.portal.kernel.service.LayoutFriendlyURLLocalService;

public class LayoutFriendlyURLServiceTracker extends ServiceTracker<LayoutFriendlyURLLocalService,LayoutFriendlyURLLocalService> {
    public LayoutFriendlyURLServiceTracker(BundleContext bundleContext){
        super(bundleContext, LayoutFriendlyURLLocalService.class, null);
    }
}

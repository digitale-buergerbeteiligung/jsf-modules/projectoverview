package beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.portlet.PortletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.WebKeys;

import ideaService.model.Category;
import ideaService.service.CategoryLocalService;
import model.CategoryPojo;
import projectService.model.Project;
import projectService.service.ProjectLocalService;
import servicetrackers.CategoryLocalServiceTracker;
import servicetrackers.DLAppLocalServiceTracker;
import servicetrackers.LayoutLocalServiceTracker;
import servicetrackers.ProjectLocalServiceTracker;
import servicetrackers.UserLocalServiceTracker;

@ManagedBean(name="projectOverviewBean")
@SessionScoped
public class ProjectOverviewBean implements Serializable {

	private static final long serialVersionUID = -5964206511781720132L;
	private static final String FILE_TITLE = "PROJECT_TITLE_PICTURE";

	private UserLocalServiceTracker userLocalServiceTracker;
	private DLAppLocalServiceTracker dlAppLocalServiceTracker;
	private ProjectLocalServiceTracker projectLocalServiceTracker;
	private LayoutLocalServiceTracker layoutLocalServiceTracker;
	private CategoryLocalServiceTracker categoryLocalSerivceTracker;
	private List<Project> allProjects;
	private String newProjectTitle;
	private String newProjectDescription;
	private boolean newProjectPublish;
	private UploadedFile titlePic;
	private File currentPicture;
	private List<CategoryPojo> categories;


	public void onPageLoad(){
		updateProjects();
	}

	private void updateProjects() {
		 ProjectLocalService projectLocalServiceUtil = projectLocalServiceTracker.getService();
		 this.allProjects = projectLocalServiceUtil.getAllProjects();
		 if(this.allProjects.equals(null)){
			 this.allProjects = new ArrayList<Project>();
		 }
	}

	public List<Project> getAllProjects(){
		return this.allProjects;
	}



	@PostConstruct
    public void postConstruct() {
        Bundle bundle = FrameworkUtil.getBundle(this.getClass());
        BundleContext bundleContext = bundle.getBundleContext();
        userLocalServiceTracker = new UserLocalServiceTracker(bundleContext);
        dlAppLocalServiceTracker = new DLAppLocalServiceTracker(bundleContext);
        projectLocalServiceTracker = new ProjectLocalServiceTracker(bundleContext);
        layoutLocalServiceTracker = new LayoutLocalServiceTracker(bundleContext);
        categoryLocalSerivceTracker = new CategoryLocalServiceTracker(bundleContext);
        dlAppLocalServiceTracker.open();
        userLocalServiceTracker.open();
        projectLocalServiceTracker.open();
        layoutLocalServiceTracker.open();
        categoryLocalSerivceTracker.open();
        categories = new ArrayList<CategoryPojo>();
    }

    @PreDestroy
    public void preDestroy() {
        userLocalServiceTracker.close();
        dlAppLocalServiceTracker.close();
        projectLocalServiceTracker.close();
        layoutLocalServiceTracker.close();
        categoryLocalSerivceTracker.close();
    }

	public void deleteOnClick(long id){
		ProjectLocalService projectLocalServiceUtil = projectLocalServiceTracker.getService();
			projectLocalServiceUtil.deleteProjectAndLayoutOnCascade(id);

			RequestContext.getCurrentInstance().update("projectoverviewform");
	}

	public boolean isAdmin(){
		try {
			User user = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			for(Role c : user.getRoles()){
				if(c.getName().equals(RoleConstants.ADMINISTRATOR)){
					//is admin
					return true;
				}
			}
			//Not admin
			return false;
		} catch (NumberFormatException | PortalException e) {
			//Not logged in
			return false;
		}
	}

	public long createNewProject() throws PortalException, IOException{
		ProjectLocalService projectLocalServiceUtil = projectLocalServiceTracker.getService();
		LayoutLocalService layoutLocalService  = layoutLocalServiceTracker.getService();
		User user = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
		PortletRequest portletRequest = (PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebKeys.THEME_DISPLAY);
		ServiceContext serviceContext = ServiceContextFactory.getInstance(Layout.class.getName(), portletRequest);
		FileEntry createdFileEntry = null;
		if(this.titlePic != null){
			createdFileEntry = saveFile(this.titlePic);
		}
		String titleImgRef = themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId() + "/" + createdFileEntry.getFolderId() +  "/" +createdFileEntry.getTitle();
		Layout page = layoutLocalService.addLayout(user.getUserId(), themeDisplay.getScopeGroupId(), false, 0, newProjectTitle, newProjectTitle,
				newProjectDescription, LayoutConstants.TYPE_PORTLET, false, "/"+this.newProjectTitle, serviceContext);

		Project newProject = projectLocalServiceUtil.createProjectWithAutomatedDbId(themeDisplay.getScopeGroupId(), user.getCompanyId(),
				user.getUserId(),newProjectTitle, newProjectDescription, newProjectPublish,titleImgRef,page.getFriendlyURL(),page.getPrimaryKey(),createdFileEntry.getPrimaryKey());
		projectLocalServiceUtil.persistProjectAndPerformTypeChecks(newProject);
		if(this.currentPicture.exists()){
			this.currentPicture.delete();
		}

		//Save all Categories
		if(!categories.isEmpty())
		for(CategoryPojo c : categories){
			saveCategory(newProject.getPrimaryKey(),user,themeDisplay,c);
		}

		newProjectTitle = null;
		newProjectDescription = null;
		titlePic = null;
		createdFileEntry = null;

		RequestContext.getCurrentInstance().update("projectInputModalForm");
		RequestContext.getCurrentInstance().update("projectoverviewform");
		return newProject.getPrimaryKey();
	}

	private void saveCategory(long projectId, User u, ThemeDisplay t, CategoryPojo cat){
		CategoryLocalService categoryService = categoryLocalSerivceTracker.getService();
		Category newCat = categoryService.createNewCategoryWithAutomatedDBId(u.getUserId(),t.getScopeGroupId() , cat.getCategoryDescription() , cat.getCategoryName(),projectId);
		newCat.persist();
	}

	private User getCurrentUser(String id) throws NumberFormatException, PortalException{
		UserLocalService userLocalService = userLocalServiceTracker.getService();
		return userLocalService.getUserById(Long.parseLong(id));
	}

    public void upload (FileUploadEvent event) throws IOException {
        this.setTitlePic(event.getFile());
        }

    public void addCategory() {
    	categories.add(new CategoryPojo());
    	RequestContext.getCurrentInstance().update("projectInputModalForm");

    }

    public void removeCategory(CategoryPojo cat) {
        categories.remove(cat);
    }




	/**
	 * @return the categories
	 */
	public List<CategoryPojo> getCategories() {
		return categories;
	}

	/**
	 * @param categories the categories to set
	 */
	public void setCategories(List<CategoryPojo> categories) {
		this.categories = categories;
	}

	/**
	 * @return the newProjectTitle
	 */
	public String getNewProjectTitle() {
		return newProjectTitle;
	}

	/**
	 * @param newProjectTitle the newProjectTitle to set
	 */
	public void setNewProjectTitle(String newProjectTitle) {
		this.newProjectTitle = newProjectTitle;
	}

	/**
	 * @return the newProjectDescription
	 */
	public String getNewProjectDescription() {
		return newProjectDescription;
	}

	/**
	 * @param newProjectDescription the newProjectDescription to set
	 */
	public void setNewProjectDescription(String newProjectDescription) {
		this.newProjectDescription = newProjectDescription;
	}

	/**
	 * @return the newProjectPublish
	 */
	public boolean isNewProjectPublish() {
		return newProjectPublish;
	}

	/**
	 * @param newProjectPublish the newProjectPublish to set
	 */
	public void setNewProjectPublish(boolean newProjectPublish) {
		this.newProjectPublish = newProjectPublish;
	}
	/**
	 * @return the currentPicture
	 */
	public File getCurrentPicture() {
		return currentPicture;
	}

	/**
	 * @param currentPicture the currentPicture to set
	 */
	public void setCurrentPicture(File currentPicture) {
		this.currentPicture = currentPicture;
	}

	/**
	 * @return the titlePic
	 */
	public UploadedFile getTitlePic() {
		return titlePic;
	}

	/**
	 * @param titlePic the titlePic to set
	 */
	public void setTitlePic(UploadedFile titlePic) {
		this.titlePic = titlePic;
	}


	@Deprecated
	public String getHeight(){
		if(this.allProjects.isEmpty()){
			return "50px";
		}else{
			return ((Math.ceil(this.allProjects.size()) /2) * 435 ) + "px";
		}
	}


//////////////////////////////////////////////////////////////////////////////////////////
///////////////////// DUPLICATE CODE FROM IDEAINPUTBEAN FOR FILE UPLOADING //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

    private FileEntry saveFile(UploadedFile file) throws PortalException, IOException{
    	UserLocalService userService = userLocalServiceTracker.getService();
    	currentPicture = convertUploadedFileToFile(file);

    	PortletRequest portletRequest = (PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    	ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebKeys.THEME_DISPLAY);

    	Folder folder = createFolderOrGetExistingFolder(themeDisplay,portletRequest, "testfolder", "testfolder");
    	long userId = Long.parseLong(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
    	User user = userService.getUser(userId);
    	String title = user.getScreenName() + FILE_TITLE  +
    			FilenameUtils.removeExtension(file.getFileName()) + UUID.randomUUID ();

    	return fileUpload(currentPicture, user,title,folder.getName(),themeDisplay,portletRequest);
    }

	private Folder createFolderOrGetExistingFolder(ThemeDisplay themeDisplay,PortletRequest request, String name, String description) throws PortalException{
		DLAppLocalService dlappLocalService = dlAppLocalServiceTracker.getService();

		//TODO error if not logged in.
		long userId = Long.parseLong(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
		long repositoryId = themeDisplay.getScopeGroupId();//repository id is same as groupId
		long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID; // or 0

			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(), request);
			Folder folder;
			try{
				folder = dlappLocalService.addFolder(userId, repositoryId, parentFolderId, name, description, serviceContext);
			}catch(Exception e){
				folder = dlappLocalService.getFolder(repositoryId, parentFolderId, name);
			}
			return folder;
	}

	public FileEntry fileUpload(File file,User user,String title,String folderName ,ThemeDisplay themeDisplay,PortletRequest request){
    	DLAppLocalService dlappLocalService = dlAppLocalServiceTracker.getService();
		long repositoryId = themeDisplay.getScopeGroupId();
	 	String mimeType = MimeTypesUtil.getContentType(file);
		String description = "This file was added programatically and only exists for testing purposes.";
		String changeLog = "changes";
		Long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
	    try
	    {
	    	Folder folder = dlappLocalService.getFolder(themeDisplay.getScopeGroupId(), parentFolderId, folderName);
	    	ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(), request);
	    	InputStream is = new FileInputStream( file );
	    	FileEntry createdEntry = dlappLocalService.addFileEntry(user.getUserId(), repositoryId, folder.getFolderId(),
	    			file.getName(), mimeType, title, description, changeLog, is, file.length(), serviceContext);
	    	return  createdEntry;
	     } catch (Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
	}

    private File convertUploadedFileToFile(UploadedFile uFile) throws IOException{
    	InputStream is = uFile.getInputstream();
    	File result = new File(uFile.getFileName());
    	FileUtils.copyInputStreamToFile(is, result);
    	return result;
    }

}
